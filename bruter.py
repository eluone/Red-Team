#!/usr/bin/python
"""A simple Web App Brute scanner"""
# Author: Tim Cumming
# Created: 15/01/22
# Modified: 15/01/22

import queue
import requests
import sys
import threading
import logging
from pathlib import Path

user_agent = "Mozilla/5.0 (X11; Linux x86_64; rv:19.0) Gecko/20100101 Firefox/19.0"
ext_list = ['.php', '.bak', '.orig', '.inc']


def setup_logger(logdir, name):
    file_formatter = logging.Formatter('%(asctime)s~%(levelname)s~%(message)s~module:%(module)s')
    console_formatter = logging.Formatter('%(levelname)s -- %(message)s')

    file_handler = logging.FileHandler(logdir / "bruter_logfile.log")
    file_handler.setLevel(logging.INFO)
    file_handler.setFormatter(file_formatter)
    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.WARNING)
    console_handler.setFormatter(console_formatter)

    logger = logging.getLogger()
    logger.addHandler(file_handler)
    logger.addHandler(console_handler)
    logger.setLevel(logging.INFO)

    return logger


# Setup log destination and formatter
log_dir = Path.cwd()
logger = setup_logger(log_dir, __name__)


def get_words(word_file, resume=None):
    def extend_words(word):
        if "." in word:
            words.put(f'/{word}')
        else:
            words.put(f'/{word}/')

        for extension in ext_list:
            words.put(f'/{word}{extension}')

    with open(word_file) as f:
        raw_words = f.read()
    found_resume = False
    words = queue.Queue()
    for word in raw_words.split():
        if resume is not None:
            if found_resume:
                extend_words(word)
            elif word == resume:
                found_resume = True
                print(f'Resuming wordlist from: {resume}')
        else:
            print(word)
            extend_words(word)
    return words


def dir_bruter(target, words):
    headers = {'User-Agent': user_agent}
    while not words.empty():
        url = f'{target}{words.get()}'
        try:
            r = requests.get(url, headers=headers)
        except requests.exceptions.ConnectionError:
            logger.critical(f'Connection Error ({url})')
            continue

        if r.status_code == 200:
            logger.info(f'Success ({r.status_code}: {url})')
        elif r.status_code == 404:
            sys.stderr.write('.')
            sys.stderr.flush()
        else:
            logger.warning(f'{r.status_code} => {url}')


def main():
    # Correct command line arguments or print usage
    if len(sys.argv[1:]) != 3:
        # TODO: Add [extension list] [custom agent string]
        print("Usage:   ./bruter.py [target] [threads] [word list]")
        print("Example: ./bruter.py http://testphp.vulnweb.com 50 ~/Words/all.txt")
        sys.exit(0)

    # Setup target options
    target, threads = sys.argv[1], int(sys.argv[2])

    # Setup remote target options
    word_file = sys.argv[3]

    words = get_words(word_file)
    print('Press Return to Continue...')
    sys.stdin.readline()
    for _ in range(threads):
        t = threading.Thread(target=dir_bruter, args=(target, words,))
        t.start()


if __name__ == '__main__':
    main()
