#!/usr/bin/python
"""A simple TCP server script"""
# Author: Tim Cumming
# Created: 20/09/21
# Modified: 27/09/21

import socket
import threading

bind_ip = "0.0.0.0"
bind_port = 9999


def main():
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    server.bind((bind_ip, bind_port))
    server.listen(5)

    print("[*] Listening on %s:%d" % (bind_ip, bind_port))

    while True:
        client, addr = server.accept()
        print("[*] Accepted Connection from %s:%d" % (addr[0], addr[1]))

        # setup our client thread to handle incoming data
        client_handler = threading.Thread(target=handle_client, args=(client,))
        client_handler.start()


# Client handling thread
def handle_client(client_socket):
    with client_socket as sock:
        # print out what the client has sent
        request = sock.recv(1024)
        print("[*] Received: %s" % request)

        # Send back a packet
        sock.send(b"ACK")


if __name__ == '__main__':
    main()
