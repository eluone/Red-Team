#!/usr/bin/python
"""A simple UDP client script"""
# Author: Tim Cumming
# Created: 20/09/21
# Modified: 21/09/21

import socket

target_host = "127.0.0.1"
target_port = 80

# create socket object
client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# connect not required for UDP
# send some data
client.sendto(b"AABBBCCCC", (target_host, target_port))

# receive some data
data, addr = client.recvfrom(4096)

print(data, addr)
