#!/usr/bin/python
"""A simple TCP proxy"""
# Author: Tim Cumming
# Created: 23/09/21
# Modified: 27/09/21

import sys
import socket
import threading


def server_loop(local_host, local_port, remote_host, remote_port, receive_first):

    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
        server.bind((local_host, local_port))
    except Exception as e:
        print("[!] Failed to listen on %s:%d" % (local_host, local_port))
        print("[!] Check for other listening sockets or correct permissions.")
        print(e)
        sys.exit(0)

    print("[*] Listening on %s:%d" % (local_host, local_port))

    server.listen(5)

    while True:
        client_socket, addr = server.accept()

        print("[>] Received Incoming Connection From %s:%d" % (addr[0], addr[1]))

        # Startup a thread to talk to the remote host
        proxy_thread = threading.Thread(target=proxy_handler,
                                        args=(client_socket, remote_host, remote_port, receive_first))

        proxy_thread.start()


def proxy_handler(client_socket, remote_host, remote_port, receive_first):
    # Connect to remote
    remote_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    remote_socket.connect((remote_host, remote_port))

    # Receive some data from the remote if needed (ie FTP banner)
    if receive_first:
        remote_buffer = receive_from(remote_socket)
        hexdump(remote_buffer)

        # Send to our response handler
        remote_buffer = response_handler(remote_buffer)

        # If we have data, send it to the local client
        if len(remote_buffer):
            print("[<] Sending %d bytes to localhost." % len(remote_buffer))
            client_socket.send(remote_buffer)

    # Loop over 1. Read Local > 2. Send to Remote > 3. Send to Local
    while True:
        # Read local
        local_buffer = receive_from(client_socket)

        if len(local_buffer):
            print("[>] Received %d bytes from localhost." % len(local_buffer))
            hexdump(local_buffer)

            # Send to our request handler
            local_buffer = request_handler(local_buffer)

            # Send to Remote
            remote_socket.send(local_buffer)
            print("[>] Sent to remote.")

        # Receive back response
        remote_buffer = receive_from(remote_socket)

        if len(remote_buffer):
            print("[<] Received %d bytes from remote." % len(remote_buffer))
            hexdump(remote_buffer)

            # Send to our response handler
            remote_buffer = response_handler(remote_buffer)

            # Send to Local
            client_socket.send(remote_buffer)
            print("[<] Sent to localhost.")

        # If no more data on either side, close connections
        if not len(local_buffer) or not len(remote_buffer):
            client_socket.close()
            remote_socket.close()
            print("[*] No more data. Closing connections.")
            break


def hexdump(src, length=16, sep='.'):
    result = []

    for i in range(0, len(src), length):
        sub_src = src[i:i + length]
        hexa = ''
        for h in range(0, len(sub_src)):
            if h == length / 2:
                hexa += ' '
            h = sub_src[h]
            if not isinstance(h, int):
                h = ord(h)
            h = hex(h).replace('0x', '')
            if len(h) == 1:
                h = '0' + h
            hexa += h + ' '
        hexa = hexa.strip(' ')
        text = ''
        for c in sub_src:
            if not isinstance(c, int):
                c = ord(c)
            if 0x20 <= c < 0x7F:
                text += chr(c)
            else:
                text += sep
        result.append(('%08X:  %-' + str(length * (2 + 1) + 1) + 's  |%s|') % (i, hexa, text))

    print('\n'.join(result))


def receive_from(connection):
    buffer = b""

    # Default to a 2 second timeout (May be too aggressive if slow connection)
    connection.settimeout(2)

    try:
        # Read buffer until there is no more data or we have timed out
        while True:
            data = connection.recv(4096)

            if not data:
                break

            buffer += data

    except Exception as e:
        print('error ', e)
        pass

    return buffer


def request_handler(buffer):
    # Packet modifications
    return buffer


def response_handler(buffer):
    # Packet modifications
    return buffer


def main():
    # Correct command line arguments or print usage
    if len(sys.argv[1:]) != 5:
        print("Usage:   ./tcp_proxy.py [localhost] [local port] [remote host] [remote port] [receive first]")
        print("Example: ./tcp_proxy.py 127.0.0.1 9000 10.12.132.1 9000 True")
        sys.exit(0)

    # Setup local options
    local_host, local_port = sys.argv[1], int(sys.argv[2])

    # Setup remote target options
    remote_host, remote_port = sys.argv[3], int(sys.argv[4])

    # Should we connect and receive before sending to remote?
    receive_first = sys.argv[5]

    if "True" in receive_first:
        receive_first = True
    else:
        receive_first = False

    # Startup the listening socket
    server_loop(local_host, local_port, remote_host, remote_port, receive_first)


if __name__ == '__main__':
    main()
